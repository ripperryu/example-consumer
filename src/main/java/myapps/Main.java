package myapps;

public class Main {

    public static void main(String... args)  {
        PostgresqlJdbc postgresqlJdbc = new PostgresqlJdbc();
        //postgresqlJdbc.connect();
        ExampleKafkaConsumer kafkaConsumer = new ExampleKafkaConsumer();
        kafkaConsumer.runKafkaConsumer();
    }
}
