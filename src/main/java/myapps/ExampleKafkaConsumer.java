package myapps;

import org.apache.kafka.clients.consumer.*;
import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.common.serialization.LongDeserializer;
import org.apache.kafka.common.serialization.StringDeserializer;

import java.util.*;

import org.apache.commons.lang.ArrayUtils;


public class ExampleKafkaConsumer
{
    private final static String BOOTSTRAP_SERVER = "localhost:9092";
    private final static String TOPIC = "AsynchronousProducerTopic";

    /**
     * This method creates the kafkaConsumer.
     * It uses the BOOTSTRAP_SERVER and TOPIC as properties for the consumer configuration.
     * @return kafkaConsumer with the right properties and subscribed to topic.
     */
    private Consumer<Long, String> createKafkaConsumer() {
        final Properties props = new Properties();

        //adding server information and topic information into props
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG,
                BOOTSTRAP_SERVER);
        props.put(ConsumerConfig.GROUP_ID_CONFIG,
                "KafkaExampleConsumer");
        //adding LongDeserializer for keys and StringDeserializer for Value
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG,
                LongDeserializer.class.getName());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG,
                StringDeserializer.class.getName());

        // creating KafkaConsumer with the defined properties (props)
        final Consumer<Long, String> kafkaConsumer = new KafkaConsumer<>(props);

        // Subscribe kafkaConsumer to the topic
        kafkaConsumer.subscribe(Collections.singletonList(TOPIC));
        return kafkaConsumer;
    }

    public void runKafkaConsumer() {
        final Consumer<Long, String> kafkaConsumer = createKafkaConsumer();

        final int timeout = 50;
        int noRecordsCount = 0;

        PostgresqlJdbc postgres = new PostgresqlJdbc();
        while (true) {
            //poll method is used to retrieve records from brokers
            //poll timeout = 500 milliseconds
            final ConsumerRecords<Long, String> consumerRecords = kafkaConsumer.poll(500);

            if (consumerRecords.count()==0) {
                noRecordsCount++;
                if (noRecordsCount > timeout) break;
                else continue;
            } else {
                noRecordsCount = 0;
            }

            consumerRecords.forEach(record -> {
                System.out.printf("Consumer Record:(%s, %d)\n", record.value(), record.offset());
                List<String> recordList= parseRecord(record.value());
                try {
                    postgres.databaseConnection(recordList);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            });
            kafkaConsumer.commitAsync();
        }
        kafkaConsumer.close();
        System.out.println("DONE");
    }

    //TODO Match the regex to the real log files
    public List<String> parseRecord(String record){
        record = record.replaceAll("(?<=[a-zA-Z])(\\s+?)(?=[a-zA-Z]|[3])", "_");
        //record = record.replaceAll("\\s+","");
        record = record.replaceAll(",", "");
        record = record.replaceAll("\":\"", " ");
        record = record.replaceAll("\"", "");
        record = record.replaceAll("\\{", "");
        record = record.replaceAll("}", "");
        record = record.replaceAll("}", "");
        record = record.replaceAll("-", " ");
        String[] recordList = record.split("\\s+");
        List<String> newRecordList = new ArrayList<String>(Arrays.asList(recordList));

        //remove unwanted items in list
        newRecordList.remove(newRecordList.get(2));
        newRecordList.remove(newRecordList.get(2));
        return newRecordList;
    }

}