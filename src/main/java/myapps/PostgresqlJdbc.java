package myapps;

import org.apache.kafka.common.protocol.types.Field;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class PostgresqlJdbc {

    private final static String URL = "jdbc:postgresql://localhost:5432/Oplog";
    private final static String USER = "postgres";
    private final static String PASSWORD = "Sandmonster1.";
    private final static String SCHEMA = "oplog";
    private static final String CREATE_TABLE_SQL="CREATE TABLE test ("
            + "UID INT NOT NULL,"
            + "NAME VARCHAR(45) NOT NULL,"
            + "DOB DATE NOT NULL,"
            + "EMAIL VARCHAR(45) NOT NULL,"
            + "PRIMARY KEY (UID))";

    public String databaseConnection(List<String> recordList) throws Exception {
        Connection connection;
        Class.forName("org.postgresql.Driver");
        connection = DriverManager.getConnection(URL, USER, PASSWORD);
        Statement stmt = connection.createStatement();

        //build the name of the table
        String tableName = recordList.get(1) + "_" + recordList.get(2);
        tableName = tableName.toLowerCase();
        //remove unwanted Elements
        //remove device name
        recordList.remove(recordList.get(1));
        String jsonDescription = recordList.get(1);
        //remove all occurrences of json description
        while(recordList.contains(jsonDescription)) {
            recordList.remove(jsonDescription);
        }

        DatabaseMetaData databaseMetaData = connection.getMetaData();

        //get all table names and put them in a list
        ResultSet tables = databaseMetaData.getTables(null, null, tableName,
                new String[] {"TABLE"});
        List<String> tablesList = new ArrayList<>();
        while (tables.next()) {
            tablesList.add(tables.getString("TABLE_NAME"));
            System.out.println(tables.getString("TABLE_NAME"));
        }
        tables.close();

        String timeStamp = recordList.get(0);

        //check if table exists and create queries
        if(!tablesList.contains(tableName)) {
            String createTable = "CREATE TABLE "+ SCHEMA + "." + tableName + " ("
                    + "ID SERIAL ,"
                    + "TIMESTAMP VARCHAR (45),";

            //save timestamp and then remove from list
            recordList.remove(recordList.get(0));
            //gets name and skips value
            for(int i = 0; i < recordList.size(); i = i + 2) {
                createTable = createTable + recordList.get(i) + " VARCHAR (45),";
            }
            createTable = createTable + "PRIMARY KEY (ID))";

            System.out.println(createTable);
            stmt.executeUpdate(createTable);
        }
        //create insert query
        String insert = "INSERT INTO " + SCHEMA + "." + tableName + " (timestamp,";
        String insertValues = " VALUES ('" + timeStamp + "','";
        for(int i = 2; i <= recordList.size(); i = i + 2) {
            //check if last item
            if(i + 1 >= recordList.size()) {
                insertValues = insertValues + recordList.get(i) +"');";
                insert = insert + recordList.get(i - 1) +")";
            }
            else {
                insert = insert + recordList.get(i - 1) + ",";
                insertValues = insertValues + recordList.get(i) + "','";
            }
        }
        insert = insert + insertValues;
        System.out.println(insert);
        stmt.executeUpdate(insert);
        connection.close();
        return tableName;
    }


}
